# Docker Compose PostGIS and pgAdmin4

## Requirements

* Docker >= 17
* Docker Compose >= 1.17

## TL;DR

To start:

```
cd [this project directory]
docker-compose up -d
```

## Configuration

Settings are controlled by environment variables:

**PostgreSQL Variables**

* `POSTGRES_IMAGE`: The docker image for postgres.  Default: `postgis/postgis:latest`
* `POSTGRES_PORT`: The port to expose for postgres.  Default: `5432`
* `POSTGRES_VOLUME`:  The docker volume (name or path) where postgres data will be stored.  Default: `postgres`
* `POSTGRES_USER`: The postgres admin username.  Default: `postgres`
* `POSTGRES_PASSWORD`: The postgres admin password.  Default: `changeme`

**pgAdmin4 Variables**

* `PGADMIN_IMAGE`: The docker image for pgadmin.  Default: `dpage/pgadmin4:latest`
* `PGADMIN_PORT`: The port to expose for pgadmin.  Default: `8000`
* `PGADMIN_VOLUME`:  The docker volume (name or path) where pgadmin data will be stored.  Default: `pgadmin`
* `PGADMIN_DEFAULT_EMAIL`: The pgadmin username.  Default: `pgadmin`
* `PGADMIN_DEFAULT_PASSWORD`: The pgadmin password.  Default: `changeme`

## Connecting to PostgreSQL

PostgreSQL will be started at the following:

* host: `localhost`
* port: `5432`
* username: `postgres`
* password: `changeme`

## Connecting to pgAdmin

pgAdmin4 can be acessed at the following:

* host: `http://localhost:8000`
* username: `pgadmin`
* password: `changeme`

## Adding PostgreSQL Server to pgAdmin4

After connecting to pgAdmin4, add a `Server` with the following config:

* host name/address: `postgres`
* port: `5432`
* username: `postgres`
* password: `changeme`
